/*
Copyright 2024 oldyu.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"

	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	devopsv1beta1 "gitlab.com/go-course-project/go13/operator/api/v1beta1"
)

// DnsRecordReconciler reconciles a DnsRecord object
type DnsRecordReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=devops.go13.org,resources=dnsrecords,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=devops.go13.org,resources=dnsrecords/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=devops.go13.org,resources=dnsrecords/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the DnsRecord object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.17.3/pkg/reconcile
func (r *DnsRecordReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	l := log.FromContext(ctx)

	// TODO(user): your logic here
	// 1. 获取对象, json Unmarshal
	obj := &devopsv1beta1.DnsRecord{}
	if err := r.Get(ctx, req.NamespacedName, obj); err != nil {
		if apierrors.IsNotFound(err) {
			// 执行删除逻辑, {module: "dnsrecord"}
			l.Info("执行删除逻辑", "module", "dnsrecord")
		}
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	l.Info("获取对象成功", "service", obj.Spec.Service, "domain", obj.Spec.Domain)

	// 2. 判断对象状态
	if obj.Status.Stage > devopsv1beta1.STAGE_WAITING {
		l.Info("已经解析")
		return ctrl.Result{}, nil
	}

	// 3. 达成期望
	l.Info("调用DNS来进行解析, 调用DNS API查看是否已经解析")
	// 更加具体的执行结果修改状态
	obj.Status.Stage = devopsv1beta1.STAGE_SUCCESS

	// 更新状态, 通过update方法来更新对象
	if err := r.Update(ctx, obj); err != nil {
		l.Error(err, "update dns record error")
	}

	l.Info("调用DNS来进行解析成功")
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *DnsRecordReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&devopsv1beta1.DnsRecord{}).
		Complete(r)
}
