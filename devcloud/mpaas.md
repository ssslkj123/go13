# 发布平台


## 服务源代码

+ [](https://gitlab.com/infraboard/cicd_test)


## 需要SDK哪些能力

部署需要的能力:
+ Workload:  CreateDeploy
+ Service: CreateService
+ Config: CreateConfigMap

维护部署的能力:
+ Pod日志: WatchLog
+ Pod登录: Exec

更新的能力: Operator
+ Workload:
+ ServiceLoad:
+ Config:


### 环境的准备

需要准备一个k8s集群: 
+ kubeadmin 安装一个, 比较重
+ 买一个云商的托管服务(按量付费), 几块钱(建议)
+ 状一个本地的 k3s, 比轻量级
+ 本地安装Docker Desktop, 使用它自带的k8s(建议)

```sh
Client Version: v1.29.1
Kustomize Version: v5.0.4-0.20230601165947-6ce0bf390ce3
Server Version: v1.29.1
```

### SDK 基本能力, 如何认证

最常用就是基于 kubeconf的认证

Kubeconfig Flie --> Kubeconfig Object ---> Restful Config ----> Client
```go
func NewClient(kubeConfigYaml string) (*Client, error) {
	// 加载kubeconfig配置
	kubeConf, err := clientcmd.Load([]byte(kubeConfigYaml))
	if err != nil {
		return nil, err
	}

	// 构造Restclient Config
	restConf, err := clientcmd.BuildConfigFromKubeconfigGetter("",
		func() (*clientcmdapi.Config, error) {
			return kubeConf, nil
		},
	)
	if err != nil {
		return nil, err
	}

	// 初始化客户端
	client, err := kubernetes.NewForConfig(restConf)
	if err != nil {
		return nil, err
	}

	return &Client{
		kubeconf: kubeConf,
		restconf: restConf,
		client:   client,
		log:      log.Sub("provider.k8s"),
	}, nil
}
```

```sh
➜  mpaas git:(master) ✗ kubectl api-resources     
NAME                              SHORTNAMES   APIVERSION                        NAMESPACED   KIND
bindings                                       v1                                true         Binding
componentstatuses                 cs           v1                                false        ComponentStatus
configmaps                        cm           v1                                true         ConfigMap
endpoints                         ep           v1                                true         Endpoints
events                            ev           v1                                true         Event
limitranges                       limits       v1                                true         LimitRange
namespaces                        ns           v1                                false        Namespace
nodes                             no           v1                                false        Node
persistentvolumeclaims            pvc          v1                                true         PersistentVolumeClaim
persistentvolumes                 pv           v1                                false        PersistentVolume
pods                              po           v1                                true         Pod
podtemplates                                   v1                                true         PodTemplate
replicationcontrollers            rc           v1                                true         ReplicationController
resourcequotas                    quota        v1                                true         ResourceQuota
secrets                                        v1                                true         Secret
serviceaccounts                   sa           v1                                true         ServiceAccount
services                          svc          v1                                true         Service
mutatingwebhookconfigurations                  admissionregistration.k8s.io/v1   false        MutatingWebhookConfiguration
validatingwebhookconfigurations                admissionregistration.k8s.io/v1   false        ValidatingWebhookConfiguration
customresourcedefinitions         crd,crds     apiextensions.k8s.io/v1           false        CustomResourceDefinition
apiservices                                    apiregistration.k8s.io/v1         false        APIService
controllerrevisions                            apps/v1                           true         ControllerRevision
daemonsets                        ds           apps/v1                           true         DaemonSet
deployments                       deploy       apps/v1                           true         Deployment
replicasets                       rs           apps/v1                           true         ReplicaSet
statefulsets                      sts          apps/v1                           true         StatefulSet
selfsubjectreviews                             authentication.k8s.io/v1          false        SelfSubjectReview
tokenreviews                                   authentication.k8s.io/v1          false        TokenReview
localsubjectaccessreviews                      authorization.k8s.io/v1           true         LocalSubjectAccessReview
selfsubjectaccessreviews                       authorization.k8s.io/v1           false        SelfSubjectAccessReview
selfsubjectrulesreviews                        authorization.k8s.io/v1           false        SelfSubjectRulesReview
subjectaccessreviews                           authorization.k8s.io/v1           false        SubjectAccessReview
horizontalpodautoscalers          hpa          autoscaling/v2                    true         HorizontalPodAutoscaler
cronjobs                          cj           batch/v1                          true         CronJob
jobs                                           batch/v1                          true         Job
certificatesigningrequests        csr          certificates.k8s.io/v1            false        CertificateSigningRequest
leases                                         coordination.k8s.io/v1            true         Lease
endpointslices                                 discovery.k8s.io/v1               true         EndpointSlice
events                            ev           events.k8s.io/v1                  true         Event
flowschemas                                    flowcontrol.apiserver.k8s.io/v1   false        FlowSchema
prioritylevelconfigurations                    flowcontrol.apiserver.k8s.io/v1   false        PriorityLevelConfiguration
ingressclasses                                 networking.k8s.io/v1              false        IngressClass
ingresses                         ing          networking.k8s.io/v1              true         Ingress
networkpolicies                   netpol       networking.k8s.io/v1              true         NetworkPolicy
runtimeclasses                                 node.k8s.io/v1                    false        RuntimeClass
poddisruptionbudgets              pdb          policy/v1                         true         PodDisruptionBudget
clusterrolebindings                            rbac.authorization.k8s.io/v1      false        ClusterRoleBinding
clusterroles                                   rbac.authorization.k8s.io/v1      false        ClusterRole
rolebindings                                   rbac.authorization.k8s.io/v1      true         RoleBinding
roles                                          rbac.authorization.k8s.io/v1      true         Role
priorityclasses                   pc           scheduling.k8s.io/v1              false        PriorityClass
csidrivers                                     storage.k8s.io/v1                 false        CSIDriver
csinodes                                       storage.k8s.io/v1                 false        CSINode
csistoragecapacities                           storage.k8s.io/v1                 true         CSIStorageCapacity
storageclasses                    sc           storage.k8s.io/v1                 false        StorageClass
volumeattachments                              storage.k8s.io/v1                 false        VolumeAttachment
```

```go
func (c *Client) ServerVersion() (string, error) {
	si, err := c.client.ServerVersion()
	if err != nil {
		return "", err
	}

	return si.String(), nil
}
```

### 部署需要的能力

+ Workload:  CreateDeploy
```go
func (c *Client) GetDeployment(ctx context.Context, req *meta.GetRequest) (*appsv1.Deployment, error) {
	d, err := c.appsv1.Deployments(req.Namespace).Get(ctx, req.Name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	d.APIVersion = "apps/v1"
	d.Kind = "Deployment"
	return d, nil
}

func (c *Client) WatchDeployment(ctx context.Context, req *appsv1.Deployment) (watch.Interface, error) {
	return c.appsv1.Deployments(req.Namespace).Watch(ctx, metav1.ListOptions{})
}

func (c *Client) CreateDeployment(ctx context.Context, req *appsv1.Deployment) (*appsv1.Deployment, error) {
	return c.appsv1.Deployments(req.Namespace).Create(ctx, req, metav1.CreateOptions{})
}

func (c *Client) UpdateDeployment(ctx context.Context, req *appsv1.Deployment) (*appsv1.Deployment, error) {
	return c.appsv1.Deployments(req.Namespace).Update(ctx, req, metav1.UpdateOptions{})
}

func (c *Client) ScaleDeployment(ctx context.Context, req *meta.ScaleRequest) (*v1.Scale, error) {
	return c.appsv1.Deployments(req.Scale.Namespace).UpdateScale(ctx, req.Scale.Name, req.Scale, req.Options)
}
```

```go
func TestCreateDeployment(t *testing.T) {
	req := &v1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      "nginx",
			Namespace: "default",
		},
		Spec: v1.DeploymentSpec{
			Replicas: tea.Int32(2),
			Selector: &metav1.LabelSelector{
				MatchLabels: map[string]string{"k8s-app": "nginx"},
			},
			Strategy: v1.DeploymentStrategy{
				Type: v1.RollingUpdateDeploymentStrategyType,
				RollingUpdate: &v1.RollingUpdateDeployment{
					MaxSurge:       k8s.NewIntStr(1),
					MaxUnavailable: k8s.NewIntStr(0),
				},
			},
			// Pod模板参数
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Annotations: map[string]string{},
					Labels: map[string]string{
						"k8s-app": "nginx",
					},
				},
				Spec: corev1.PodSpec{
					// Pod参数
					DNSPolicy:                     corev1.DNSClusterFirst,
					RestartPolicy:                 corev1.RestartPolicyAlways,
					SchedulerName:                 "default-scheduler",
					TerminationGracePeriodSeconds: tea.Int64(30),
					// Container参数
					Containers: []corev1.Container{
						{
							Name:            "nginx",
							Image:           "nginx:latest",
							ImagePullPolicy: corev1.PullAlways,
							Env: []corev1.EnvVar{
								{Name: "APP_NAME", Value: "nginx"},
								{Name: "APP_VERSION", Value: "v1"},
							},
							Resources: corev1.ResourceRequirements{
								Limits: corev1.ResourceList{
									corev1.ResourceCPU:    resource.MustParse("500m"),
									corev1.ResourceMemory: resource.MustParse("1Gi"),
								},
								Requests: corev1.ResourceList{
									corev1.ResourceCPU:    resource.MustParse("50m"),
									corev1.ResourceMemory: resource.MustParse("50Mi"),
								},
							},
						},
					},
				},
			},
		},
	}

	yamlReq, err := yaml.Marshal(req)
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(string(yamlReq))

	d, err := impl.CreateDeployment(ctx, req)
	if err != nil {
		t.Log(err)
	}
	t.Log(d)
}
```

+ Service: CreateService
+ Config: CreateConfigMap


### operator更新回状态

server UpdateDeploymentStatus 更新 当前 Workload 和 Pod的状态
```go
func (i *impl) UpdateDeploymentStatus(ctx context.Context, in *deploy.UpdateDeploymentStatusRequest) (
	*deploy.Deployment, error) {
	req := deploy.NewDescribeDeploymentRequest(in.Id)
	ins, err := i.DescribeDeployment(ctx, req)
	if err != nil {
		return nil, err
	}

	if err := ins.ValidateToken(in.UpdateToken); err != nil {
		return nil, err
	}

	i.log.Debug().Msgf("更新部署%s状态", ins.Spec.Name)
	switch ins.Spec.Type {
	case deploy.TYPE_KUBERNETES:
		err := i.UpdateK8sDeployStatus(ctx, ins, in.UpdatedK8SConfig)
		if err != nil {
			return nil, err
		}
	}

	// Active时重新刷新Pod列表
	if ins.Status.Stage == deploy.STAGE_ACTIVE {
		syncReq := NewSyncK8sDeployRequest()
		syncReq.SyncDeployment = false
		err := i.SyncK8sDeploy(ctx, syncReq, ins)
		if err != nil {
			return nil, err
		}
	}

	// 更新
	_, err = i.col.UpdateOne(ctx, bson.M{"_id": ins.Meta.Id}, bson.M{"$set": ins})
	if err != nil {
		return nil, exception.NewInternalServerError("update deploy status(%s) error, %s", ins.Meta.Id, err)
	}

	return ins, nil
}
```


operator里面反向同步 Deployment对象
```go
func (r *Reconciler) HandleDeploy(ctx context.Context, obj appsv1.Deployment) error {
	// 获取日志对象
	l := log.FromContext(ctx)

	// 根据注解获取task id
	deployId := obj.Annotations[deploy.ANNOTATION_DEPLOY_ID]
	if deployId == "" {
		return nil
	}
	l.Info(fmt.Sprintf("get mpaas deploy: %s", deployId))

	// 查询Deploy
	ins, err := r.mpaas.Deploy().DescribeDeployment(ctx, deploy.NewDescribeDeploymentRequest(deployId))
	if err != nil {
		return fmt.Errorf("get deploy error, %s", err)
	}

	// 更新Depoy
	updateReq := deploy.NewUpdateDeploymentStatusRequest(deployId)
	if ins.Credential != nil {
		updateReq.UpdateToken = ins.Credential.Token
	}

	updateReq.UpdatedK8SConfig.WorkloadKind = workload.WORKLOAD_KIND_DEPLOYMENT.String()
	updateReq.UpdatedK8SConfig.WorkloadConfig = format.MustToYaml(obj)
	updateReq.UpdateBy = r.name
	_, err = r.mpaas.Deploy().UpdateDeploymentStatus(ctx, updateReq)
	if err != nil {
		return err
	}

	return nil
}
```

反向同步Pod对象
```go
func (r *PodReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	// 获取日志对象
	l := log.FromContext(ctx)

	// 1.通过名称获取Pod对象, 并打印
	var obj v1.Pod
	if err := r.Get(ctx, req.NamespacedName, &obj); err != nil {
		// 如果Pod对象不存在就删除该Pod
		if apierrors.IsNotFound(err) {
			r.DeletePod(ctx, req.Namespace, req.Name)
		}
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	if obj.Labels == nil {
		obj.Labels = map[string]string{}
	}

	// 根据Job标签, 更新JobTask状态
	if err := r.HandleJobTask(ctx, obj); err != nil {
		l.Error(err, "update mflow job task error")
	}

	// 更新Deploy注解 更新Deploy
	if err := r.HandleDeploy(ctx, obj); err != nil {
		l.Error(err, "update mpaas deploy error")
	}

	return ctrl.Result{}, nil
}
```


### 关于WebConsole

+ ConsoleUI (Terminal) 
+ Websocket Server (Tcp Stream): mpaas api
+ Pod Socket

### Pod日志获取

1. 获取Pod日志
怎么通过socket 获取Pod的日志

```go
// 查看容器日志
func (c *Client) WatchConainterLog(ctx context.Context, req *WatchConainterLogRequest) (io.ReadCloser, error) {
	restReq := c.corev1.Pods(req.Namespace).GetLogs(req.PodName, req.PodLogOptions)
	return restReq.Stream(ctx)
}
```

```go
func TestWatchConainterLog(t *testing.T) {
	req := workload.NewWatchConainterLogRequest()
	req.Namespace = "default"
	req.PodName = "cicd-test-8f97775-h78sj"
	stream, err := impl.WatchConainterLog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	defer stream.Close()
	_, err = io.Copy(os.Stdout, stream)
	if err != nil {
		t.Fatal(err)
	}
}
```

2. 把io日志流 copy到 websocket: io.Copy(websocket, stream)
```go
// Watch Container Log Websocket
func (h *websocketHandler) WatchConainterLog(r *restful.Request, w *restful.Response) {
	ws, err := upgrader.Upgrade(w, r.Request, nil)
	if err != nil {
		response.Failed(w, err)
		return
	}

	websocket.Subprotocols(r.Request)
	term := terminal.NewWebSocketTerminal(ws)

	// 开启认证与鉴权
	entry := endpoint.NewEntryFromRestRequest(r).
		SetAuthEnable(true).
		SetPermissionEnable(true)
	err = middleware.Get().PermissionCheck(r, w, entry)
	if err != nil {
		term.Failed(err)
		return
	}

	// 获取参数
	req := workload.NewWatchConainterLogRequest()
	if err = term.ReadReq(req); err != nil {
		term.Failed(err)
		return
	}
	req.PodName = r.PathParameter("name")

	client := r.Attribute(proxy.ATTRIBUTE_K8S_CLIENT).(*k8s.Client)
	reader, err := client.WorkLoad().WatchConainterLog(r.Request.Context(), req)
	if err != nil {
		term.Failed(err)
		return
	}

	// 读取出来的数据流 copy到term
	_, err = io.Copy(term, reader)
	if err != nil {
		term.Failed(err)
		return
	}

	term.Success("ok")
}
```

从websoccket连接中获取参数: ()
```go
func (i *WebSocketWriter) ReadReq(req any) error {
	mt, data, err := i.ws.ReadMessage()
	if err != nil {
		return err
	}
	if mt != websocket.TextMessage {
		return fmt.Errorf("req must be TextMessage, but now not, is %d", mt)
	}
	if !json.Valid(data) {
		return fmt.Errorf("req must be json data, but %s", string(data))
	}

	return json.Unmarshal(data, req)
}

// 具体参数
type WatchConainterLogRequest struct {
	Namespace string `json:"namespace" validate:"required"`
	PodName   string `json:"pod_name" validate:"required"`
	*v1.PodLogOptions
}
```



### Pod登录 Console

1. Pod Stream 如何获取(input/output): Exector ---> ContainerTerminal

```go
// 登录容器
func (c *Client) LoginContainer(ctx context.Context, req *LoginContainerRequest) error {
	restReq := c.corev1.RESTClient().Post().
		Resource("pods").
		Name(req.PodName).
		Namespace(req.Namespace).
		SubResource("exec")

	restReq.VersionedParams(&v1.PodExecOptions{
		Container: req.ContainerName,
		Command:   req.Command,
		Stdin:     true,
		Stdout:    true,
		Stderr:    true,
		TTY:       true,
	}, scheme.ParameterCodec)

	executor, err := remotecommand.NewSPDYExecutor(c.restconf, "POST", restReq.URL())
	if err != nil {
		return err
	}

	return executor.StreamWithContext(ctx, remotecommand.StreamOptions{
		Stdin:             req.Excutor,
		Stdout:            req.Excutor,
		Stderr:            req.Excutor,
		Tty:               true,
		TerminalSizeQueue: req.Excutor,
	})
}
```

2. ContainerTerminal 的定义
```go
type ContainerTerminal interface {
	io.Reader // stdin  <--- websocket <--- web termianl
	io.Writer // stdin/stdout --->  websocket --> web terminal
	remotecommand.TerminalSizeQueue
}
```

3. 如何把 ContainerTerminal 和 Websocket集合起来
```go
// Login Container Websocket
func (h *websocketHandler) LoginContainer(r *restful.Request, w *restful.Response) {
	ws, err := upgrader.Upgrade(w, r.Request, nil)
	if err != nil {
		response.Failed(w, err)
		return
	}

	term := terminal.NewWebSocketTerminal(ws)
	// term.SetAuditor(os.Stdout)

	// 开启认证与鉴权
	entry := endpoint.NewEntryFromRestRequest(r).
		SetAuthEnable(true).
		SetPermissionEnable(true)

	err = middleware.Get().PermissionCheck(r, w, entry)
	if err != nil {
		term.Failed(err)
		return
	}

	// 获取参数
	req := workload.NewLoginContainerRequest(term)
	if err = term.ReadReq(req); err != nil {
		term.Failed(err)
		return
	}

	// 登录容器
	client := r.Attribute(proxy.ATTRIBUTE_K8S_CLIENT).(*k8s.Client)
	err = client.WorkLoad().LoginContainer(r.Request.Context(), req)
	if err != nil {
		term.Failed(err)
		return
	}

	term.Success("ok")
}
```


### Web Termianl: xterm前端

xterm文档: http://xtermjs.org/

1. Container Log
```html
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="node_modules/xterm/css/xterm.css" />
    <script src="node_modules/xterm/lib/xterm.js"></script>
    <script src="./term.js"></script>
    <title>Document</title>
</head>

<body>
    <dir style="    margin: 8;padding: 0;">
        <span>Cluster Id: <input name="cluster_id" type="text" value="k8s-test"></span>
        <span>Namespace: <input name="namespace" type="text" value="default"></span>
        <span>Pod Name: <input type="text" name="pod_name" value="mcenter-56d7d5f568-66xkt"></span>
        <span>Container Name: <input type="text" name="container_name" value="mcenter"></span>
        <span>Token: <input type="text" name="token" value="JnOf3ezUweAmjG3ynjxGYtv5"></span>
        <span><button onclick="connect()" id="connect">确认</button></span>
    </dir>
    <div id="terminal" style="height: 95vh;"></div>
    <script>
        // Terminal UI
        var term = new Terminal({
            theme: Solarized_Darcula,
            fontSize: 13,
            convertEol: true,
            disableStdin: false
        });
        term.open(document.getElementById('terminal'));

        // 调整窗口
        const fitSize = () => {
            var geometry = getTermSize(term)
            term.resize(geometry.cols, geometry.rows)
        }
        // 终端大小调整
        window.onresize = fitSize
        const connBtn = document.getElementById("connect")


        // Terminal Sockeet
        const connect = () => {
            const inputs = document.getElementsByTagName("input")
            const cluster_id = inputs['cluster_id'].value
            const namespace = inputs['namespace'].value
            const pod_name = inputs['pod_name'].value
            const container_name = inputs['container_name'].value
            const token = inputs['token'].value
            let socket = new WebSocket(`ws://127.0.0.1:8090/mflow/api/v1/proxy/${cluster_id}/pods/${pod_name}/log?mcenter_access_token=${token}`);
            socket.onopen = function (e) {
                socket.send(JSON.stringify({ namespace, pod_name, container:container_name }));
                fitSize()
            };
            socket.onmessage = function (event) {
                if (event.data instanceof Blob) {
                    // 数据
                    let reader = new FileReader();
                    reader.onload = e => {
                        term.write(e.target.result)
                    };
                    reader.readAsText(event.data)
                } else {
                    // 指令响应
                    console.log(event.data)
                }
            };
            socket.onclose = function (event) {
                console.log(event)
                if (event.wasClean) {
                    term.write(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
                } else {
                    // e.g. server process killed or network down
                    // event.code is usually 1006 in this case
                    term.write(`[close] Connection died, code=${event.code}`);
                }
            };
            socket.onerror = function (error) {
                term.write(`[error]`);
            };
            connBtn.disabled = true
        }

    </script>
</body>

</html>
```

2. Container Login
```html
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="node_modules/xterm/css/xterm.css" />
    <script src="node_modules/xterm/lib/xterm.js"></script>
    <script src="./term.js"></script>
    <title>Document</title>
</head>

<body>
    <dir style="    margin: 8;padding: 0;">
        <span>Cluster Id: <input name="cluster_id" type="text" value="k8s-test"></span>
        <span>Namespace: <input name="namespace" type="text" value="default"></span>
        <span>Pod Name: <input type="text" name="pod_name" value="mcenter-56d7d5f568-66xkt"></span>
        <span>Container Name: <input type="text" name="container_name" value="mcenter"></span>
        <span>Token: <input type="text" name="token" value="JnOf3ezUweAmjG3ynjxGYtv5"></span>
        <span><button onclick="connect()" id="connect">确认</button></span>
    </dir>
    <div id="terminal" style="height: 95vh;"></div>
    <script>
        // Terminal UI
        var term = new Terminal({
            theme: Solarized_Darcula,
            fontSize: 13,
            convertEol: true
        });
        term.open(document.getElementById('terminal'));



        // Terminal Socket
        const connBtn = document.getElementById("connect")
        const connect = () => {
            const inputs = document.getElementsByTagName("input")
            const cluster_id = inputs['cluster_id'].value
            const namespace = inputs['namespace'].value
            const pod_name = inputs['pod_name'].value
            const container_name = inputs['container_name'].value
            const token = inputs['token'].value
            let socket = new WebSocket(`ws://127.0.0.1:8090/mflow/api/v1/proxy/${cluster_id}/pods/${pod_name}/login?mcenter_access_token=${token}`);
            //心跳检测
            var heartCheck = {
                timeout: 10000,        //10秒发一次心跳
                timeoutObj: null,
                reset: function () {
                    clearTimeout(this.timeoutObj);
                    return this;
                },
                start: function () {
                    this.timeoutObj = setTimeout(function () {
                        //这里发送一个心跳，后端收到后，返回一个心跳消息，
                        //onmessage拿到返回的心跳就说明连接正常
                        socket.send(JSON.stringify({command:"ping",params: {}}))
                    }, this.timeout)
                }
            }

            socket.onopen = function (e) {
                connBtn.disabled = true
                socket.send(JSON.stringify({ namespace, pod_name, container:container_name }));
                fitSize()
            };
            socket.onmessage = function (event) {
                //如果获取到消息，心跳检测重置, 拿到任何消息都说明当前连接是正常的
                heartCheck.reset().start();

                if (event.data instanceof Blob) {
                    // 数据
                    let reader = new FileReader();
                    reader.onload = e => {
                        term.write(e.target.result)
                    };
                    reader.readAsText(event.data)
                } else {
                    // 指令响应
                    console.log(event.data)
                }
            };
            socket.onclose = function (event) {
                heartCheck.reset()
                connBtn.disabled = false
                if (event.wasClean) {
                    term.write(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
                } else {
                    // e.g. server process killed or network down
                    // event.code is usually 1006 in this case
                    term.write('[close] Connection died');
                }
            };
            socket.onerror = function (error) {
                term.write(`[error]`);
            };


            // 终端设置, 用户输入
            term.onData(send => {
                // 数据都使用bytes
                const encoder = new TextEncoder();
                const arrayBuffer = encoder.encode(send).buffer;
                socket.send(arrayBuffer)
            })

            // 调整窗口
            const fitSize = () => {
                var geometry = getTermSize(term)
                term.resize(geometry.cols, geometry.rows)
                socket.send(JSON.stringify({ command: "resize", params: { width: geometry.cols, heigh: geometry.rows } }))
            }
            // 终端大小调整
            window.onresize = fitSize


        }
    </script>
</body>

</html>
```