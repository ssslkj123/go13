package main

import (
	"fmt"
	"log"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"

	"gitlab.com/go-course-project/go13/skills/rpc/helloworld/service"
)

// HelloService{}.Greet()  ---> HelloService.Greet
// 127.0.0.1:1234.HelloService.Greet ---> HelloService{}.Greet()
func main() {
	client, err := NewClient("localhost:1234")
	if err != nil {
		log.Fatal("dialing:", err)
	}
	var resp string
	err = client.Greet("bob", &resp)
	if err != nil {
		panic(err)
	}
	fmt.Println(resp)
}

// 实现接口的服务实例
var _ service.HelloService = (*Client)(nil)

func NewClient(server string) (*Client, error) {
	// 建立链接
	conn, err := net.Dial("tcp", "localhost:1234")
	if err != nil {
		log.Fatal("net.Dial:", err)
	}

	// 采用Json编解码的客户端
	c := rpc.NewClientWithCodec(jsonrpc.NewClientCodec(conn))
	return &Client{
		client: c,
	}, nil

	// echo -e '{"method":"HelloService.Greet","params":["bob"],"id":1}' | nc localhost 1234
	// {"id":1,"result":"hello:hello","error":null}
}

type Client struct {
	client *rpc.Client
}

func (c *Client) Greet(requeset string, response *string) error {
	// 然后通过client.Call调用具体的RPC方法
	// 在调用client.Call时:
	// 		第一个参数是用点号链接的RPC服务名字和方法名字，
	// 		第二个参数是 请求参数
	//      第三个是请求响应, 必须是一个指针, 有底层rpc服务帮你赋值
	// Restful  post  http://127.0.0.1:8080/helloservice/greent {param: {}}  {}
	err := c.client.Call("HelloService.Greet", "bob", response)
	if err != nil {
		log.Fatal(err)
	}
	return nil
}
