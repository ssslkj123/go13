package conf

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"sync"

	"google.golang.org/grpc"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// 这里不采用直接暴露变量的方式, 比较好的方式 使用函数
var config *Config

// 这里就可以补充逻辑
func C() *Config {
	// sync.Lock
	if config == nil {
		// 给个默认值
		config = DefaultConfig()
	}
	return config
}

func DefaultConfig() *Config {
	return &Config{
		Application: &Application{
			Domain: "127.0.0.1",
		},
		MySQL: &MySQL{
			Host:     "127.0.0.1",
			Port:     3306,
			DB:       "vblog",
			Username: "root",
			Password: "123456",
			Debug:    true,
		},
		GrpcServer: &GrpcServer{
			Host: "127.0.0.1",
			Port: 1234,
		},
	}
}

// 程序配置对象, 启动时 会读取配置， 并且为程序提供需要全局变量
// 把配置对象做出全局变量(单列模式)
//
// toml
/*
[mysql]
host="127.0.0.1"
port=3306
...
*/
type Config struct {
	Application *Application `json:"app" yaml:"app" toml:"app"`
	MySQL       *MySQL       `json:"mysql" yaml:"mysql" toml:"mysql"`
	GrpcServer  *GrpcServer  `json:"grpc" yaml:"grpc" toml:"grpc"`
}

type Application struct {
	Domain string `json:"domain" yaml:"domain" toml:"domain" env:"APP_DOMAIN"`
}

type GrpcServer struct {
	Host   string `json:"host" yaml:"host" toml:"host"`
	Port   int    `json:"port" yaml:"port" toml:"port"`
	server *grpc.Server
}

func (s *GrpcServer) GetServer() *grpc.Server {
	if s.server == nil {
		s.server = grpc.NewServer()
	}
	return s.server
}

func (s *GrpcServer) Address() string {
	return fmt.Sprintf("%s:%d", s.Host, s.Port)
}

func (s *GrpcServer) Start() {
	lis, err := net.Listen("tcp", s.Address())
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("grpc server listen: %s\n", s.Address())
	err = s.server.Serve(lis)
	if err != nil {
		log.Fatal(err)
	}
}

//	fmt.Stringer
//
// 如果你想要自定义 对象fmt.PrintXXX() 打印的值
// String() string
// &{0x1400007c600} ---> JSON {}
func (c *Config) String() string {
	jd, err := json.MarshalIndent(c, "", "  ")
	if err != nil {
		return fmt.Sprintf("%p", c)
	}
	return string(jd)
}

// db对象也是一个单列模式
type MySQL struct {
	Host     string `json:"host" yaml:"host" toml:"host" env:"DATASOURCE_HOST"`
	Port     int    `json:"port" yaml:"port" toml:"port" env:"DATASOURCE_PORT"`
	DB       string `json:"database" yaml:"database" toml:"database" env:"DATASOURCE_DB"`
	Username string `json:"username" yaml:"username" toml:"username" env:"DATASOURCE_USERNAME"`
	Password string `json:"password" yaml:"password" toml:"password" env:"DATASOURCE_PASSWORD"`
	Debug    bool   `json:"debug" yaml:"debug" toml:"debug" env:"DATASOURCE_DEBUG"`

	// 判断这个私有属性, 来判断是否返回已有的对象
	db *gorm.DB
	l  sync.Mutex
}

// dsn := "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
func (m *MySQL) DSN() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		m.Username,
		m.Password,
		m.Host,
		m.Port,
		m.DB,
	)
}

// 通过配置就能通过一个DB 实例
func (m *MySQL) GetDB() *gorm.DB {
	m.l.Lock()
	defer m.l.Unlock()

	if m.db == nil {
		db, err := gorm.Open(mysql.Open(m.DSN()), &gorm.Config{})
		if err != nil {
			panic(err)
		}
		m.db = db

		// 补充Debug配置
		if m.Debug {
			m.db = db.Debug()
		}
	}

	return m.db
}

// 配置对象提供全局单列配置
func (c *Config) DB() *gorm.DB {
	return c.MySQL.GetDB()
}
