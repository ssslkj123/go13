package comment_test

import (
	"encoding/json"
	"testing"

	"gitlab.com/go-course-project/go13/vblog/apps/comment"
)

func TestComment(t *testing.T) {
	ins := &comment.Comment{
		Spec: &comment.CreateCommnetRequest{
			Content: "test",
		},
	}

	// {"spec":{"content":"test"}}
	jd, _ := json.Marshal(ins)
	t.Log(string(jd))

	// {"id":"","create_at":0,"content":"test"}
	jd2, _ := json.Marshal(struct {
		Id       string `json:"id"`
		CreateAt int64  `json:"create_at"`
		*comment.CreateCommnetRequest
	}{
		Id:                   ins.Id,
		CreateAt:             ins.CreateAt,
		CreateCommnetRequest: ins.Spec,
	})
	t.Log(string(jd2))
}
