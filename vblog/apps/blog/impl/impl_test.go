package impl_test

import (
	"context"

	"gitlab.com/go-course-project/go13/vblog/apps/blog"
	"gitlab.com/go-course-project/go13/vblog/ioc"

	// 1. 加载对象
	_ "gitlab.com/go-course-project/go13/vblog/apps"
)

// blog service 的实现的具体对象是在ioc中,
// 需要在ioc中获取具体的svc 用来测试

var (
	impl blog.Service
	ctx  = context.Background()
)

func init() {
	// 2. ioc获取对象
	impl = ioc.Controller().Get(blog.AppName).(blog.Service)

	// ioc需要初始化 才能填充 db属性
	if err := ioc.Init(); err != nil {
		panic(err)
	}
}
